*** Settings ***
Test Setup        Open Url    ${baseUrl}    ${browser}
Test Teardown     End Test
Force Tags        mj
Library           Selenium2Library
Resource          ../../Settings/Base_Settings.txt
Resource          ../../Keywords/Base_Keywords.txt
Resource          ../../Keywords/Cart_Keywords.txt
Resource          ../../Keywords/Product_Detail_Keywords.txt
Resource          ../../Objects/Main_Page.txt
Resource          ../../Objects/User_Page.txt

*** Test Cases ***
AddItemToCart
    [Documentation]    Req05_T03
    OpenMainCategory    Women
    OpenNthProduct    2
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1

CheckElementsOfCart
    [Documentation]    Req06 - rozsirit?
    OpenMainCategory    Women
    OpenNthProduct    2
    LoadInfoAboutProduct
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenShoppingCart
    ${x}=    Get Variable Value    ${product_name_value}
    ${y}=    Get Variable Value    ${product_price_value}
    CheckFirstProductNameAndDescription    ${x}    ${y}

RemoveProductFromCart
    [Documentation]    Req07
    OpenMainCategory    Women
    OpenNthProduct    1
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenShoppingCart
    Click Element    ${icon_trash}
    Page Should Contain    (empty)
    Click Element    ${logo}
    OpenMainCategory    Women
    OpenNthProduct    2
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenShoppingCart

AddQuantity
    [Documentation]    Req08_T1
    OpenMainCategory    Women
    OpenNthProduct    1
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenShoppingCart
    Click Element    ${icon_plus}
    CheckQuantityChangeAdd    2

RemoveQuantity
    [Documentation]    Req08_T2
    OpenMainCategory    Women
    OpenNthProduct    1
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenMainCategory    Women
    OpenNthProduct    1
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    2
    OpenShoppingCart
    CheckQuantityChangeAdd    2
    Click Element    ${icon_minus}
    CheckQuantityChangeAdd    1

CheckRules
    [Documentation]    Req09
    Login To Eshop    ${login_un}    ${login_pw}
    OpenMainCategory    Women
    OpenNthProduct    1
    AddProductToCart
    OpenShoppingCart
    Click Element    ${proceed_button}
    Click Element    ${proceed_button_address}
    Click Element    ${proceed_button_carrier}
    Page Should Contain    You must agree to the terms of service before continuing.

CompleteOrder
    [Documentation]    Req10
    Login To Eshop    ${login_un}    ${login_pw}
    OpenMainCategory    Women
    OpenNthProduct    1
    AddProductToCart
    OpenShoppingCart
    Click Element    ${proceed_button}
    Click Element    ${proceed_button_address}
    Click Element    ${tos_check}
    Click Element    ${proceed_button_carrier}
    Click Element    ${payment_bankwire}
    Click Element    //button/span[contains(text(),"I confirm my order")]
    Page Should Contain    Your order on My Store is complete.

CheckAdress
    [Documentation]    Req11_T1
    Login To Eshop    ${login_un}    ${login_pw}
    OpenMainCategory    Women
    OpenNthProduct    2
    LoadInfoAboutProduct
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenShoppingCart
    ${x}=    Get Variable Value    ${product_name_value}
    ${y}=    Get Variable Value    ${product_price_value}
    CheckFirstProductNameAndDescription    ${x}    ${y}
    Click Element    ${proceed_button}
    Page Should Contain Element    //li[contains(@class,"third") and contains(@class,"step_current")]

CheckAskLogin
    [Documentation]    Req11_T2
    OpenMainCategory    Women
    OpenNthProduct    2
    LoadInfoAboutProduct
    AddProductToCart
    Element Text Should Be    ${s_c_quantity}    1
    OpenShoppingCart
    ${x}=    Get Variable Value    ${product_name_value}
    ${y}=    Get Variable Value    ${product_price_value}
    CheckFirstProductNameAndDescription    ${x}    ${y}
    Click Element    ${proceed_button}
    Page Should Contain Element    //li[contains(@class,"second") and contains(@class,"step_current")]
