*** Settings ***
Library    Selenium2Library
Test Setup    Open Url    ${baseUrl}    ${browser}
Test Teardown    End Test
Resource    ../../Settings/Base_Settings.txt
Resource    ../../Keywords/Base_Keywords.txt
Resource    ../../Objects/Main_Page.txt
Resource    ../../Objects/ForgotPassword_Page.txt

*** Test Cases ***
#REQ04_T1
Check Forgot password
    Click Element    ${login_label}
    Click Element    //a[contains(text(), "Forgot your password?")]
    Page Should Contain    Please enter the email address you used to register. We will then send you a new password.
    Page Should Contain Element    ${fp_email_input}
    Page Should Contain Element    ${fp_submit_button}
    #check invalid email
    Input Text    ${fp_email_input}    something
    Click Element    ${fp_submit_button}
    Wait Until Page Contains Element    //div[@class="alert alert-danger"]/ol/li[contains(text(), "Invalid email address.")]
    Page Should Contain Element    //div[@class="alert alert-danger"]/ol/li[contains(text(), "Invalid email address.")]
    #check not registered email
    Input Text    ${fp_email_input}    not_registred4it446@example.com
    Click Element    ${fp_submit_button}
    Wait Until Page Contains Element    //div[@class="alert alert-danger"]/ol/li[contains(text(), "There is no account registered for this email address.")]
    Page Should Contain Element    //div[@class="alert alert-danger"]/ol/li[contains(text(), "There is no account registered for this email address.")]
    #check registered email
    Input Text    ${fp_email_input}    ${login_un}
    Click Element    ${fp_submit_button}
    Wait Until Page Contains Element    //p[@class="alert alert-success"]
    Element Should Contain    //p[@class="alert alert-success"]    A confirmation email has been sent to your address: ${login_un}
