*** Settings ***
Library    Selenium2Library
Test Setup    Open Url    ${baseUrl}    ${browser}
Test Teardown    End Test
Resource    ../../Settings/Base_Settings.txt
Resource    ../../Keywords/Base_Keywords.txt
Resource    ../../Objects/Main_Page.txt

*** Test Cases ***
#REQ05_T1
Check Products
    Page Should Contain Element    //a[contains(text(), "Women")]
    Page Should Contain Element    //a[contains(text(), "Dresses")]
    Page Should Contain Element    //a[contains(text(), "T-shirts")]
    Click Element    //a[contains(text(), "${category_name}")]
    ${Count}=    Get Matching Xpath Count    //ul[@class="product_list grid row"]/li
    : FOR    ${INDEX}    IN RANGE    1    ${Count} + 1    
    \    Page Should Contain Element    //ul[@class="product_list grid row"]/li[${INDEX}]
    \    #check image
    \    Page Should Contain Element    //ul[@class="product_list grid row"]/li[${INDEX}]/div/div/div[@class="product-image-container"]/a/img
    \    #check price
    \    Page Should Contain Element    //ul[@class="product_list grid row"]/li[${INDEX}]/div/div[@class="right-block"]/div[@class="content_price"]/span[@class="price product-price"]
    \    #check add to Cart button
    \    Page Should Contain Element    //ul[@class="product_list grid row"]/li[${INDEX}]/div/div[@class="right-block"]/div[@class="button-container"]/a[contains(., "Add to cart")]
    \    #check more button
    \    Page Should Contain Element    //ul[@class="product_list grid row"]/li[${INDEX}]/div/div[@class="right-block"]/div[@class="button-container"]/a[contains(., "More")]
