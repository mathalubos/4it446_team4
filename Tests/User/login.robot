*** Settings ***
Library    Selenium2Library
Test Setup    Open Url    ${baseUrl}    ${browser}
Test Teardown    End Test
Resource    ../../Settings/Base_Settings.txt
Resource    ../../Keywords/Base_Keywords.txt
Resource    ../../Objects/Main_Page.txt
Resource    ../../Objects/User_Page.txt
Resource    ../../Objects/Login_Page.txt

*** Test Cases ***
#REQ01_T1
Check Login
    Check Login Elements
#REQ01_T2
Check Product Login
    Click Element    //img[@title="Blouse"]
    Check Login Elements
#REQ01_T3
Check Cart Login
    Click Element    //div/a[@title="View my shopping cart"]
    Check Login Elements
#REQ02_T1
Check Valid Email Forgot Password
    Check Login Elements
    Input Text    ${email_create_input}    something
    Click Element    ${email_create_submit}
    Wait Until Page Contains Element    ${create_user_error}
    Page Should Contain Element    ${create_user_error}
    Input Text    ${login_email_input}    something
    Click Element    ${login_email_submit}
    Wait Until Page Contains Element    ${login_error}
    Page Should Contain Element    ${login_error}
    Click Element    //a[contains(text(), "Forgot your password?")]
    Capture Page Screenshot
#REQ03_T1
Successful Login
    Login To Eshop    ${login_un}    ${login_pw}
    Wait Until Page Contains Element    ${profile_label}
    Element Should Contain    ${profile_label}    ${full_username}
    Capture Page Screenshot
#REQ03_T2
Failed Login
    Login To Eshop    ${login_un}    bad_pw
    Wait Until Page Contains Element    //div[@class="alert alert-danger"]/p
    Element Should Contain    //div[@class="alert alert-danger"]/p    There is 1 error
    Capture Page Screenshot
